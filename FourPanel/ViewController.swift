//
//  ViewController.swift
//  FourPanel
//
//  Created by Alina Chernenko on 5/4/16.
//  Copyright © 2016 dimalina. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var webView: UIWebView!

    @IBOutlet weak var mapView: MKMapView!
    
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let webURL = NSURL (string:"https://www.facebook.com")
        
        let request = NSURLRequest(URL: webURL!)
        
        self.webView.loadRequest(request)
        
        self.locationManager.requestAlwaysAuthorization()
        
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        //self.mapView.showsUserLocation = true
        
    }
    

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations: \(locValue.latitude) \(locValue.longitude)")
        let locationLat = locValue.latitude
        let locationLong = locValue.longitude
        
        let center = CLLocationCoordinate2DMake(locationLat, locationLong)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.mapView.setRegion(region, animated: true)
        
        let information = MKPointAnnotation()
                information.coordinate = CLLocationCoordinate2DMake(locationLat, locationLong)
                information.title = "Your location"
        
                self.mapView.addAnnotation(information)
         locationManager.stopUpdatingLocation()

        
        
    }

}

