//
//  AnotherViewController.swift
//  FourPanel
//
//  Created by Alina Chernenko on 5/17/16.
//  Copyright © 2016 dimalina. All rights reserved.
//

import UIKit

class AnotherViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var image: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.image
    }

}
